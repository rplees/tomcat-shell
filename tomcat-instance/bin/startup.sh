#!/bin/bash
TEMP_FILE_CURR="$(dirname "$0")"
if [ ${TEMP_FILE_CURR:0:1} = "." ];then
	TEMP_FILE_CURR="${TEMP_FILE_CURR#*./}"
fi

#ssh
if [ "`dirname ${PWD}`" = "/" ]; then
	PARENT_PATH=`dirname $TEMP_FILE_CURR`
elif [ $TEMP_FILE_CURR = "." ]; then
	PARENT_PATH=`dirname ${PWD}`
else
	PARENT_PATH=`dirname ${PWD}/${TEMP_FILE_CURR}`
fi

PARENT_DIR_NAME=`basename "$PARENT_PATH"`

#CATALINA_HOME需要手动设置
export CATALINA_HOME="/usr/local/apache-tomcat-8"
export CATALINA_BASE="$PARENT_PATH"
export CATALINA_TMPDIR="$CATALINA_BASE/temp"
export CATALINA_PID="$CATALINA_BASE/bin/tomcat.pid"
export JAVA_OPTS="-server -Xms1024m -Xmx1024m -Djava.awt.headless=true -Dtomcat.name=$PARENT_DIR_NAME"

echo "==================================="
echo "CATALINA_BASE---$CATALINA_BASE"
echo "tomcat.name---$PARENT_DIR_NAME"
echo "==================================="

echo $"Starting Tomcat"

#创建logs目录
if [ ! -d "$CATALINA_BASE/webapps" ]; then
	echo "webapps log dir."
  mkdir $CATALINA_BASE/webapps
fi

#创建logs目录
if [ ! -d "$CATALINA_BASE/logs" ]; then
	echo "create log dir."
  mkdir $CATALINA_BASE/logs
fi

#创建temp目录
if [ ! -d "$CATALINA_BASE/temp" ]; then
	echo "create temp dir."
  mkdir $CATALINA_BASE/temp
fi

# 调用tomcat启动脚本
bash $CATALINA_HOME/bin/startup.sh "$@"

echo $"Started Tomcat"
