#!/bin/bash
TEMP_FILE_CURR="$(dirname "$0")"
if [ ${TEMP_FILE_CURR:0:1} = "." ];then
	TEMP_FILE_CURR="${TEMP_FILE_CURR#*./}"
fi

#ssh
if [ "`dirname ${PWD}`" = "/" ]; then
	PARENT_PATH=`dirname $TEMP_FILE_CURR`
elif [ $TEMP_FILE_CURR = "." ]; then
	PARENT_PATH=`dirname ${PWD}`
else
	PARENT_PATH=`dirname ${PWD}/${TEMP_FILE_CURR}`
fi

PARENT_DIR_NAME=`basename "$PARENT_PATH"`

export CATALINA_HOME="/usr/local/apache-tomcat-8"
export CATALINA_BASE="$PARENT_PATH"
export CATALINA_TMPDIR="$CATALINA_BASE/temp"
export CATALINA_PID="$CATALINA_BASE/bin/tomcat.pid"

echo "==================================="
echo "CATALINA_BASE---$CATALINA_BASE"
echo "tomcat.name---$PARENT_DIR_NAME"
echo "==================================="

bash $CATALINA_HOME/bin/shutdown.sh "$@"
