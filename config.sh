#!/bin/sh
#config 部分
MM_SERVERS="121.40.102.44"
ROMTE_TOMCAT_INSTANCE_BASE="/usr/local/tomcat-instance"
#远程部署的实例全路径
REMOTE_TOMCAT_INSTANCE_PATH="${ROMTE_TOMCAT_INSTANCE_BASE}/tomcat-instance"
TOMCAT_INSTANCE_DIR="tomcat-instance"

if [ -n "$1" ]; then
	REMOTE_TOMCAT_INSTANCE_PATH="${ROMTE_TOMCAT_INSTANCE_BASE}/$1"
	echo "远程的目录重置为$REMOTE_TOMCAT_INSTANCE_PATH"
fi

echo "=========服务器列表==========="
for ser in ${MM_SERVERS}; do
	echo "*) ${ser}"
done
echo "=========服务器列表==========="