#!/bin/sh
. ./config.sh "$@"

LOCAL_TOMCAT_INSTALL_DIR="/usr/local/apache-tomcat-8/"
REMOTE_TOMCAT_INSTALL_DIR=LOCAL_TOMCAT_INSTALL_DIR

#创建logs目录
if [ ! -d "$LOCAL_TOMCAT_INSTALL_DIR" ]; then
  echo "请确保本地${LOCAL_TOMCAT_INSTALL_DIR}有tomcat的安装文件...."
else
	#）上传项目代码、配置脚本
	for ser in ${MM_SERVERS};do	
		if ssh root@${ser} test -e $REMOTE_TOMCAT_INSTALL_DIR; then 
			echo "服务器已经有安装的tomcat8,将跳过."
		else
			echo "安装tomcat8到${ser}"
			scp -r $LOCAL_TOMCAT_INSTALL_DIR root@${ser}:/usr/local/
		fi
	done
fi