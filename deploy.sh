#!/bin/sh
. ./config.sh "$@"
#步骤
#）备份脚本（日志、代码）
#）上传项目代码、配置脚本


#）备份脚本（日志、代码）
	#本地备份
	#远程日志备份(暂时没做，因为服务端的日志也是按日期分割的)
bak_file_name=$(date +%Y%m%d) #10260808
if [ ! -d "bak/${bak_file_name}" ]; then
  	mkdir -p bak/${bak_file_name}
else
	#今天有备份,目前是重命名，文件名格式 datename=$(date +%Y%m%d-%H%M%S) 
	bak_file_name=$(date +%Y%m%d-%H%M%S)
	echo "今天有备份，文件重命名${bak_file_name}"
	mkdir -p bak/${bak_file_name}
fi
cp -r ${TOMCAT_INSTANCE_DIR}/conf/ bak/${bak_file_name}/conf/
cp -r ${TOMCAT_INSTANCE_DIR}/deploy/ bak/${bak_file_name}/deploy/
echo "备份完成."

#）上传项目代码、配置脚本
for ser in ${MM_SERVERS};do	
	#[ ! -d "ssh root@${ser} ${REMOTE_TOMCAT_INSTANCE_PATH}" ];
	if ssh root@${ser} test -e ${REMOTE_TOMCAT_INSTANCE_PATH}; then 
		echo "Uploading ${TOMCAT_INSTANCE_DIR}/ conf and deploy to remote..."
		#全量传输
		scp -r ${TOMCAT_INSTANCE_DIR}/* root@${ser}:${REMOTE_TOMCAT_INSTANCE_PATH}/
	else
		#第一次服务器没有tomcat的实例文件
		echo "初始化项目tomcat实例文件...."
		ssh root@${ser} mkdir -p ${REMOTE_TOMCAT_INSTANCE_PATH} &&
		scp -r ${TOMCAT_INSTANCE_DIR}/* root@${ser}:${REMOTE_TOMCAT_INSTANCE_PATH}/
	fi
done
